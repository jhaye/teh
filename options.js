function saveOptions(e) {
    e.preventDefault();

    let raw_block_list = document.getElementById("block_list").value.trim();
    let raw_allow_list = document.getElementById("allow_list").value.trim();
    let all = document.getElementById("hide_all").checked;
    let split_block_list;
    let split_allow_list;


    // regular expression for removing whitespace
    let re = /\s+/;

    if (raw_block_list != "") {
	split_block_list = raw_block_list.split(re);
    } else {
	split_block_list = null;
    }

    if (raw_allow_list != "") {
	split_allow_list = raw_allow_list.split(re);
    } else {
	split_allow_list = null;
    }

    console.log(split_block_list);
    console.log(split_allow_list);
    console.log(all);

    browser.storage.local.set({
        block_list: split_block_list,
	allow_list: split_allow_list,
	hide_all: all
    });
}

function restoreOptions() {

    function setCurrentChoice(result) {
	let block_list_elem = document.getElementById("block_list");
	let allow_list_elem = document.getElementById("allow_list");
	let block_list_con;
	let allow_list_con;
	let hide_all = false;

	// format block list
	if(result.block_list == null) {
	    // default value as suggestion
	    block_list_elem.placeholder = "E.g. 'LUL Kappa'";
	} else {
	    block_list_con = result.block_list.join(" ");
	    block_list_elem.value = block_list_con;
	}

	// format allow list
	if(result.allow_list == null) {
	    // default value as suggestion
	    allow_list_elem.placeholder = "E.g. 'LUL Kappa'";
	} else {
	    allow_list_con = result.allow_list.join(" ");
	    allow_list_elem.value = allow_list_con;
	}

	if(result.hide_all != null) {
	    hide_all = result.hide_all;
	}

	block_list_elem.disabled = hide_all;
	allow_list_elem.disabled = !hide_all;


	document.getElementById("hide_all").checked = hide_all;
    }

    function onError(error) {
        console.log("Error: ${error}");
    }

    let getting = browser.storage.local.get(["block_list", "allow_list", "hide_all"]);
    getting.then(setCurrentChoice, onError);
}

function toggleDisabled() {
    let hide_all_enabled = document.getElementById("hide_all").checked;
    document.getElementById("block_list").disabled = hide_all_enabled;
    document.getElementById("allow_list").disabled = !hide_all_enabled;
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
document.getElementById("hide_all").addEventListener("change", toggleDisabled);
