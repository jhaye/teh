// Give the page time to load
//
// Since the chat on Twitch is added using JavaScript logic later on,
// we have no other option other than to wait and hope that the page
// is loaded after waiting long enough.
if(document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded",afterDOMLoaded);
} else {
    afterDOMLoaded();
}

// 5 sec delay
function afterDOMLoaded(){
    setTimeout(function() {

	// get the chat window
	target = document.getElementsByClassName("chat-list__lines").item(0);

	// halt if we don't find the chat window
	if(target == null) {
	    throw new Error("No chat window found.");
	}

	let getting = browser.storage.local.get(["block_list", "allow_list", "hide_all"]);
	getting.then(onGot, onError);

    }, 5000);
}

// our block and allow list for emotes; we'll populate it later
let block_list;
let allow_list;
// switch for hiding all emotes
let hide_all = false;
let target;

// simple element remove function
Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}

let observer = new MutationObserver(mutations => {
    mutations.forEach(mutation => {

	if(mutation.addedNodes.length == 0) {
	    return;
	}

	// gets all emotes in the mutation
        let emotes = mutation.addedNodes.item(0).getElementsByClassName(
	    "chat-line__message--emote");

	Array.from(emotes).forEach(emote => {
	    let remove_emote = false;
	    if (hide_all) {
		// allow list exists?
		if (allow_list == null) {
		    // in case of no allow list we want to hide all emotes
		    remove_emote = true;
		} else if (!allow_list.includes(emote.alt)) {
		    // in case of the emote not being in the allow list, we hide it
		    remove_emote = true;
		}
	    } else if (block_list != null) {
		// is it an emote in the block list?
		if (block_list.includes(emote.alt)) {
		    remove_emote = true;
                }
	    }

	    if (remove_emote) {
		// create equivalent text element
		let textEmote = document.createElement("span");
		textEmote.setAttribute("data-a-target",
				       "chat-message-text");
		textEmote.textContent = emote.alt;
		// insert text after it
		emote.parentElement.insertAdjacentElement("afterend",
							  textEmote);
		// get rid of it
		emote.parentElement.remove();
	    }
        });
    });
});

// observer config
let config = { childList: true, characterData: true, subtree: true };

function onError(error) {
  console.log(`Error: ${error}`);
}

function onGot(item) {
    // populate the block list
    block_list = item.block_list;
    allow_list = item.allow_list;
    hide_all = item.hide_all;
    observer.observe(target, config);
}
